/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgengine;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Norwelian
 */
public class Map {

    public String map_json;
    
    private int[][] layers;
    private Tileset[] tilesets;
    private BufferedImage[] bmpTiles;
    private int mapW, mapH;
    
    private BufferedImage[] mapRendered;

    public Map(int layersN, int mapW, int mapH, int tilesetN) {
        this.map_json = "src/rpgengine/tileset/map1.json";
        this.layers = new int[layersN][mapW * mapH];
        this.tilesets = new Tileset[tilesetN];
        this.mapW = mapW;
        this.mapH = mapH;
        jsonLayers();
        jsonTileset(); //Initialize the "tilesets" reading the json
        initBmpTiles(); //Initialize the "bmpTiles" to "new BufferedImage[tile_total_number]" 
        createBitmaps(); //Fill the "bmpTiles" from "tilesets".
    }

    private void jsonLayers() {
        JSONParser parser = new JSONParser();

        int layerN = 0;
        int n = 0;

        try {
            Object obj = parser.parse(new FileReader(this.map_json));
            JSONObject jsonObject = (JSONObject) obj;

            JSONArray msg = (JSONArray) jsonObject.get("layers");
            Iterator<JSONObject> iterator = msg.iterator();
            while (iterator.hasNext()) {
                JSONObject next = (JSONObject) iterator.next();

                msg = (JSONArray) next.get("data");

                if (msg != null) {
                    Iterator<Long> iterator1 = msg.iterator();

                    while (iterator1.hasNext()) {
                        Long value = iterator1.next();
                        if (value != null) {
                            layers[layerN][n] = value.intValue();
                        }
                        n++;
                    }
                    layerN++;
                    n = 0;
                }
            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private void jsonTileset() {
        JSONParser parser = new JSONParser();

        int tilesetN = 0;
        int[] data = new int[8 - 1]; //8 is the number of elements to be inizialized in Tileset. -1 cause one is a string.
        String dataStr = "";

        try {
            Object obj = parser.parse(new FileReader(this.map_json));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray msg = (JSONArray) jsonObject.get("tilesets");
            if (msg != null) {
                Iterator<JSONObject> iterator = msg.iterator();
                while (iterator.hasNext()) {
                    JSONObject next = (JSONObject) iterator.next();
                    if (next != null) {
                        data[0] = (int) (long) next.get("columns");
                        data[1] = (int) (long) next.get("firstgid");
                        data[2] = (int) (long) next.get("imageheight");
                        data[3] = (int) (long) next.get("imagewidth");
                        data[4] = (int) (long) next.get("tilecount");
                        data[5] = (int) (long) next.get("tileheight");
                        data[6] = (int) (long) next.get("tilewidth");
                        dataStr = (String) next.get("image");

                        tilesets[tilesetN] = new Tileset(tilesetN, data[0], data[1], dataStr, data[2], data[3], data[4], data[5], data[6]);
                        tilesetN++;
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void initBmpTiles(){
        int tilesN = 0;
        for (Tileset tileset : tilesets) {
            tilesN += tileset.getTilecount();
        }
        this.bmpTiles = new BufferedImage[tilesN];
        this.mapRendered = new BufferedImage[this.layers.length];
    }
    
    private void createBitmaps(){
        BufferedImage img = null;
        BufferedImage tmp = null;
        Graphics2D g = null;
        int x = 0, y = 0;
        try {
            for (Tileset t : this.tilesets) {
                img = ImageIO.read(new File(this.map_json.substring(0,this.map_json.lastIndexOf("/")+1) + t.getImg_source()));
                tmp = new BufferedImage(t.getTilewidth(), t.getTileheight(), BufferedImage.TYPE_INT_ARGB);
                x = 0;
                y = 0;
                for(int i = t.getFirstgid(); i < t.getFirstgid()+t.getTilecount(); i++){
                    g = tmp.createGraphics();
                    g.drawImage(img, 0, 0, t.getTilewidth(), t.getTileheight(), x, y, x+t.getTilewidth(), y+t.getTileheight(), null);
                    g.dispose();
                    
                    //System.out.println("Tileset: " + t.getId() + 
                    //        "\n\t (X,Y) = " + "(" + x + "," + y + ")" + 
                    //        "\n\t I = " + i);
                    
                    x += t.getTilewidth();
                    if((i-t.getFirstgid()+1) % (t.getColumns()) == 0 && (i-t.getFirstgid()+1) != 0){
                        y += t.getTileheight();
                        x=0;
                    }
                    this.bmpTiles[i-1] = tmp;
                    //ImageIO.write(tmp, "PNG", new File("src/rpgengine/tileset/dst/" + i + ".png")); //This is to save the tiles.
                    tmp = new BufferedImage(t.getTilewidth(), t.getTileheight(), BufferedImage.TYPE_INT_ARGB);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void print_array(int x, int y) {
        for (int n = 0; n < this.layers.length; n++) {
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < y; j++) {
                    System.out.print(this.layers[n][i * y + j] + "\t ");
                }
                System.out.println();
            }
            System.out.print("\n\n\n");
        }
    }
    
    public void drawLayers(int x, int y) {
        
        int xdiff = tilesets[0].getTilewidth();
        int ydiff = tilesets[0].getTileheight();
        
        BufferedImage map_complete = new BufferedImage(this.mapW*xdiff, this.mapH*ydiff, BufferedImage.TYPE_INT_ARGB);
        
        BufferedImage transp = new BufferedImage(xdiff, ydiff, BufferedImage.TYPE_INT_ARGB);
        AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.CLEAR, 0.0f);
        Graphics2D g2d = (Graphics2D) transp.getGraphics();
        g2d.setComposite(composite);
        g2d.setColor(new Color(0, 0, 0, 0));
        g2d.fillRect(0, 0, 10, 10);
        for (int n = 0; n < this.layers.length; n++){
            this.mapRendered[n] = new BufferedImage(this.mapW*xdiff, this.mapH*ydiff, BufferedImage.TYPE_INT_ARGB);
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < y; j++) {
                    if(this.layers[n][i * y + j] == 0)
                        this.mapRendered[n].getGraphics().drawImage(transp, j*ydiff, i*xdiff, null);
                    else
                        this.mapRendered[n].getGraphics().drawImage(this.bmpTiles[this.layers[n][i * y + j]-1], j*ydiff, i*xdiff, null);
                }
            }
            try{
                ImageIO.write(this.mapRendered[n], "PNG", new File("src/rpgengine/tileset/dst/map" + n + ".png")); //This is to save the map.
                map_complete.getGraphics().drawImage(this.mapRendered[n], 0, 0, null);
            } catch(Exception e){}
        }
        
        try{
            ImageIO.write(map_complete, "PNG", new File("src/rpgengine/tileset/dst/map.png")); //This is to save the map.
        } catch(Exception e){}
        
    }

    public static void main(String args[]) {
        //Tileset_data( layersN, mapW, mapH, tilesetN )
        Map T = new Map(2, 30, 20, 1);
        T.drawLayers(20, 30);
    }

}
