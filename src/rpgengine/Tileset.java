/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgengine;

/**
 *
 * @author Norwelian
 */
public class Tileset {
    private int id;
    private int firstgid, tilewidth, tileheight, tilecount, columns, imagewidth, imageheigth;
    private String img_source;
    
    public Tileset(int id, int columns, int firstgid, String img_source, int imageheight, int imagewidth, int tilecount, int tileheight, int tilewidth) {
        this.id = id;
        this.firstgid = firstgid;
        this.tilewidth = tilewidth;
        this.tileheight = tileheight;
        this.tilecount = tilecount;
        this.columns = columns;
        this.imagewidth = imagewidth;
        this.imageheigth = imageheight;
        this.img_source = img_source;
    }

    public int getId() {
        return id;
    }

    public int getFirstgid() {
        return firstgid;
    }

    public int getTilewidth() {
        return tilewidth;
    }

    public int getTileheight() {
        return tileheight;
    }

    public int getTilecount() {
        return tilecount;
    }

    public int getColumns() {
        return columns;
    }

    public int getImagewidth() {
        return imagewidth;
    }

    public int getImageheigth() {
        return imageheigth;
    }

    public String getImg_source() {
        return img_source;
    }
    
    @Override
    public String toString(){
        return "Tileset data:"+
                "\n- ID: " + this.id + 
                "\n- firstGid: " + this.firstgid + 
                "\n- tileCount: " + this.tilecount + 
                "\n- columns: " + this.columns +
                "\n- tileW: " + this.tilewidth + 
                "\n- tileH: " + this.tileheight + 
                "\n- img_source: " + this.img_source +
                "\n- imageW: " + this.imagewidth + 
                "\n- imageH: " + this.imageheigth;
    }
    
    
    
}
